package whisperCoaching.whisperCall;

import javax.telephony.*;
import javax.telephony.capabilities.TerminalConnectionCapabilities;
import javax.telephony.events.CallEv;
import javax.telephony.events.ProvEv;

import com.cisco.jtapi.extensions.CiscoAddress;
import com.cisco.jtapi.extensions.CiscoCall;
import com.cisco.jtapi.extensions.CiscoJtapiException;
import com.cisco.jtapi.extensions.CiscoJtapiPeer;
import com.cisco.jtapi.extensions.CiscoMonitorInitiatorInfo;
import com.cisco.jtapi.extensions.CiscoMonitorTargetInfo;
import com.cisco.jtapi.extensions.CiscoProvider;
import com.cisco.jtapi.extensions.CiscoRecorderInfo;
import com.cisco.jtapi.extensions.CiscoTerminal;
import com.cisco.jtapi.extensions.CiscoTerminalConnection;

public class CallWhisper {
	private CiscoCall call = null;
	
	public String startWhisperCoaching(Config config) {
		//creating/adding observer to the provider
        ProviderObserver observer = new ProviderObserver() {
			
			@Override
			public void providerChangedEvent(ProvEv[] arg0) {
				// TODO Auto-generated method stub
			}
		};
		
		try {
            CiscoJtapiPeer  peer = (CiscoJtapiPeer) JtapiPeerFactory.getJtapiPeer (null);
            //provider string
    		String providerString = config.getCucmUrl() + ";login=" + config.getCucmUser() + ";passwd=" + config.getCucmPws();
            CiscoProvider  provider = (CiscoProvider) peer.getProvider(providerString);//get provider based on cucm info
             
            //validate provider state
            if( provider.getState() == CiscoTerminal.OUT_OF_SERVICE ) {
            	System.out.println("PROVIDER: OUT_OF_SERVICE");
            	return "PROVIDER: OUT_OF_SERVICE";
            }else {
            	System.out.println("PROVIDER: IN_SERVICE");
            }
            
            provider.addObserver(observer);//add observer to provider
            
            //creating agent to monitor
            CiscoAddress agentAddress = (CiscoAddress) provider.getAddress(config.getToExt());
            agentAddress.addCallObserver(new CallObserver() { public void callChangedEvent(CallEv[]eventList) { } });  //add observer to agent
            CiscoTerminal agentTerm = (CiscoTerminal)agentAddress.getTerminals()[0];//creatin agent terminal
            //creating sup to monitor with
            CiscoAddress supAddress = (CiscoAddress) provider.getAddress(config.getFromExt());
            supAddress.addCallObserver(new CallObserver() { public void callChangedEvent(CallEv[]eventList) { } });
            CiscoTerminal supTerm = (CiscoTerminal)supAddress.getTerminals()[0];//creating sup terminal
            
            //create call using the provider
            call = (CiscoCall) ((CiscoProvider) agentAddress.getProvider()).createCall(); //getCall(config.getCallId());
//             call = (CiscoCall) provider.getCalls()[0];
             
            System.out.println("SEP_ID_AGENT:"+agentTerm);//TODO printing agent's terminal sep id
            
            //TODO validate agent terminal state
            if(agentTerm.getState() == CiscoTerminal.OUT_OF_SERVICE) {
//            	System.out.println("AGENT_TERMINAL: OUT_OF_SERVICE");
//            	System.out.println(agentTerm.getState());
            	return "AGENT_TERMINAL: OUT_OF_SERVICE";
            }else {
            	System.out.println("AGENT_TERMINAL: IN_SERVICE");
            	System.out.println(agentTerm.getState());
            }
            
            System.out.println("SEP_ID_SUP:"+supTerm);//TODO printing sup's terminal sep id
            
            //TODO validate sup terminal state
            if( supTerm.getState() == CiscoTerminal.OUT_OF_SERVICE) {
//            	System.out.println("SUP_TERMINAL: OUT_OF_SERVICE");
//            	System.out.println(supTerm.getState());
            	return "SUP_TERMINAL: OUT_OF_SERVICE";
            }else {
            	System.out.println("SUP_TERMINAL: IN_SERVICE");
            	System.out.println(supTerm.getState());
            }
            
            //TODO printing call info call must be IDLE to continue
            System.out.println("CALL");
            System.out.println(call);
            System.out.println(call.getState());
                       
            System.out.println( "Cisco Call try "+call);
            
            System.out.println("SUP BIB ENABLED " + supTerm.isBuiltInBridgeEnabled());
            System.out.println("SUP BIB ENABLED " + agentTerm.isBuiltInBridgeEnabled());
            
            //CiscoTerminalConnection.updateMonitorType(CiscoCall.WHISPER_MONITOR, CiscoCall.PLAYTONE_LOCALONLY);
            
            if( config.getType().equals("whisper")) {
            	 call.startMonitor( 
            			 		   supTerm, 
						           supAddress,
//						           call.getCurrentCallingTerminal().getTerminalConnections()[0],
						           agentTerm.getTerminalConnections()[0],
						           CiscoCall.WHISPER_MONITOR,
						           CiscoCall.PLAYTONE_LOCALONLY
						  		  );
            }else{
              call.startMonitor( 
	  				 		   	supTerm, 
	  					        supAddress,
	  					        //call.getCurrentCallingTerminal().getTerminalConnections()[0],
	  					        agentTerm.getTerminalConnections()[0],
	  					        CiscoCall.SILENT_MONITOR,
	  					        CiscoCall.PLAYTONE_LOCALONLY
	  					  		);
            }
            
        } catch( Exception e ) {
        	e.printStackTrace();
        	CiscoJtapiException ce = (CiscoJtapiException) e;
        	int errorCode = ce.getErrorCode();
        	String msg = ce.getErrorDescription();
        	System.out.println(msg);	
        	System.out.println(errorCode);
        	System.out.println(ce.getErrorName());
            return "Something went wrong...";
        }
		
		return "Coaching Started";
	}
	
	
}
