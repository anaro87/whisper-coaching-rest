package whisperCoaching.whisperCall;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class WhisperCallController {
	private CallWhisper whisper = new CallWhisper();
	
	@RequestMapping(value="/startCoaching", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Object>  startCoaching(@RequestBody Config config) 
	{
//		System.out.println("JSON");
//		System.out.println(config.getCucmPws());//TODO
//		System.out.println(config.getCucmUser());//TODO
//		System.out.println(config.getCucmUrl());
//		System.out.println(config.getFromExt());
//		System.out.println(config.getToExt());
		String res = whisper.startWhisperCoaching(config);
		JSONObject obj = new JSONObject();
		obj.put("result", res);
		
		System.out.println("*********");
		System.out.println(obj);
		List<JSONObject> response = new ArrayList<JSONObject>();
		response.add(obj);
		
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}
	
	@RequestMapping("/startCoaching")
	public JSONObject startCoaching(){
		Config c = new Config();
		c.setCucmUrl("172.21.101.5");
		c.setCucmUser("wispercoaching");
		c.setCucmPws("wispercoaching");
		c.setFromExt("1020");//sup
		c.setToExt("1019");//agent
		c.setType("whisper");
		c.setCallId(0);
		String res = whisper.startWhisperCoaching(c);
		JSONObject obj = new JSONObject();
		obj.put("result", res);
		
		System.out.println(res);
		
		return obj;
	}
	
	@RequestMapping("/startSilentMonitor")
	public String startSilentMonitor(){
		Config c = new Config();
		c.setCucmUrl("172.21.101.5");
		c.setCucmUser("wispercoaching");
		c.setCucmPws("wispercoaching");
		c.setFromExt("1020");//sup
		c.setToExt("1019");//agent
		c.setType("silent");
		String res = whisper.startWhisperCoaching(c);
		JSONObject obj = new JSONObject();
		obj.put("result", res);
		
		return res;
	}

}
