package whisperCoaching.whisperCall;

public class Config {
	private String cucmUrl;
	private String cucmUser;
	private String cucmPws;
	private String fromExt;//Sup DN
	private String toExt; // Agent DN to be monitored
	private String type;
	private int callId;
	
	//setters
	public void setCucmUrl(String cucmUrl) {
		this.cucmUrl = cucmUrl;
	}

	public void setCucmUser(String cucmUser) {
		this.cucmUser = cucmUser;
	}

	public void setCucmPws(String cucmPws) {
		this.cucmPws = cucmPws;
	}

	public void setFromExt(String fromExt) {
		this.fromExt = fromExt;
	}

	public void setToExt(String toExt) {
		this.toExt = toExt;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void setCallId(int callId) {
		this.callId = callId;
	}

	//getters
	public String getCucmUrl() {
		return cucmUrl;
	}

	public String getCucmUser() {
		return cucmUser;
	}

	public String getCucmPws() {
		return cucmPws;
	}

	public String getFromExt() {
		return fromExt;
	}

	public String getToExt() {
		return toExt;
	}

	public String getType() {
		return type;
	}

	public int getCallId() {
		return callId;
	}

	
}
